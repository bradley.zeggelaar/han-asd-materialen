#!/bin/ash
set -euxo pipefail
MATERIALS_REVISION="$(git rev-parse --short HEAD)"
MATERIALS_TIMESTAMP_SYSTEM="$(date -Iseconds)"
MATERIALS_EDITION="${CI_COMMIT_TAG:-$MATERIALS_REVISION}"
MATERIALS_TIMESTAMP="${CI_COMMIT_TIMESTAMP:-$MATERIALS_TIMESTAMP_SYSTEM}"
printf '%s' "$MATERIALS_EDITION" > public/version.txt
printf '%s' "$MATERIALS_TIMESTAMP" > public/timestamp.txt

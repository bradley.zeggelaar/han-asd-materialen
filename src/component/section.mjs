import { LitElement, html, css } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import { DateTime } from 'luxon';

class Section extends LitElement {
  constructor() {
    super();
    this.level = 0;
    this.index = 1;
    this.title = 'Untitled';
    // TODO: Remove?
    this.extra = false;
    this.isTopmost = false;
    this.minutesneeded = 0;
  }

  connectedCallback() {
    super.connectedCallback();
    if (this.datetimeStart) {
      this._datetimeStart = DateTime.fromISO(this.datetimeStart).setLocale('nl');
      this._timeStart = this._datetimeStart.toLocaleString(DateTime.TIME_SIMPLE);
      this._timeEnd = this._datetimeStart.plus({ minutes: this.minutesneeded }).toLocaleString(DateTime.TIME_SIMPLE);
    }
  }

  static get properties() {
    return {
      _datetimeStart: { state: true },
      _timeEnd: { state: true },
      datetimeStart: { type: String },
      extra: { type: Boolean },
      index: { type: String },
      isTopmost: { type: Boolean },
      level: { type: Number },
      minutesneeded: { type: Number },
      title: { type: String },
    };
  }

  _check(event) {
    const input = event.target;
    this.name = input.value;
    for (const child of this.children) {
      if (child !== this.firstElementChild) {
        child.hidden = !child.hidden;
      }
    }
  }

  static finalizeStyles(_styles) {
    // TODO: inherit styles
    return [
      css`
        *,
        ::before {
          box-sizing: border-box;
        }

        #header {
          display: flex;
          align-items: baseline;
          justify-content: space-between;
          text-overflow: ellipsis;
        }

        #header > div {
          display: flex;
          align-items: baseline;
          justify-content: flex-start;
        }

        #datetime {
          display: flex;
          flex-wrap: wrap;
          justify-content: flex-end;
          font-size: small;
          text-align: right;
        }

        #datetime > * {
          white-space: nowrap;
        }

        @keyframes lightspeed-in-left {
          from {
            opacity: 0;
            transform: translate3d(-100%, 0, 0) skewX(30deg);
          }

          60% {
            transform: skewX(-20deg);
          }

          80% {
            transform: skewX(5deg);
          }

          to {
            opacity: 1;
            transform: translate3d(0, 0, 0);
          }
        }

        .extra {
          min-width: 100%;
          max-width: fit-content;
          margin: 10px;
          padding: 5px;
          background-color: var(--color-extra-bg);
        }

        .extra::after {
          content: '🧐';
          position: relative;
          right: 10px;
          bottom: 0;
          font-size: 2rem;
        }
      `,
    ];
  }

  render() {
    // TODO: elementHeading.innerText.replace(regexpId, '_');
    // html`${cache(
    let checkbox = '';
    if (this.level >= 2 && this.level <= 6) {
      checkbox = html`<input type="checkbox" @change=${this._check} checked />`;
    } else {
      checkbox = html``;
    }
    /* eslint-disable no-nested-ternary */
    return html`<section class="${classMap({ extra: this.extra })}">
      <header id="header">
        <div>
          ${checkbox}
          <component-heading exportparts="a" level="${this.level}" index=${this.index}>
            <slot name="title"></slot>
          </component-heading>
        </div>
        ${this.isTopmost && this.minutesneeded && this._datetimeStart
          ? html`<p id="datetime">
              <span>📅&nbsp;${this._datetimeStart.toLocaleString(DateTime.DATETIME_FULL)}</span>&nbsp;
              <span id="timespan">⌚&nbsp;${this._timeStart}&nbsp;-&nbsp;${this._timeEnd}</span>&nbsp;
              <span>&nbsp;⌛&nbsp;${this.minutesneeded}&nbsp;m</span>
            </p>`
          : this._timeStart && this._timeEnd && this.minutesneeded
          ? html`<p id="datetime">
              <span id="timespan">⌚&nbsp;${this._timeStart}&nbsp;-&nbsp;${this._timeEnd}</span>&nbsp;
              <span id="minutesneeded">&nbsp;⌛&nbsp;${this.minutesneeded}&nbsp;m</span>
            </p>`
          : ''}
      </header>
      <slot></slot>
    </section>`;
    /* eslint-enable */
  }
}

export const componentSection = () => {
  customElements.define('component-section', Section);
};

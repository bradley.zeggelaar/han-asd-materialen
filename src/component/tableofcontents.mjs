import { LitElement, html, css } from 'lit';
import { classMap } from 'lit/directives/class-map.js';

class Tableofcontents extends LitElement {
  constructor() {
    super();
    this.sectionRoot = document.querySelector('body main > component-section');
    this.nElementDetails = document.querySelectorAll('body details');
    this.active = true;
    this.classes = { active: this.active };
  }

  static templateEntryLeaf(section, index, level) {
    let indexParent = '';
    if (section.parentElement) {
      if (section.parentElement.index) {
        indexParent = `${section.parentElement.index}.`;
      }
    }
    // TODO: clean up
    if (index && level > 2) {
      // eslint-disable-next-line no-param-reassign
      section.index = `${indexParent}${index}`;
      // eslint-disable-next-line no-param-reassign
      section.id = `sec_${section.index}`;
    } else if (index && level > 1) {
      // eslint-disable-next-line no-param-reassign
      section.index = `${index}`;
      // eslint-disable-next-line no-param-reassign
      section.id = `sec_${section.index}`;
    }
    // eslint-disable-next-line no-param-reassign
    section.level = level;
    let title = '';
    if (section.firstElementChild && section.firstElementChild.getAttribute('slot')) {
      title = section.firstElementChild.cloneNode(true);
    }
    return html`<p>
      <a part="a" href="#${section.id}"> <span class="locator-toc">${section.index}</span> </a>${title}
    </p>`;
  }

  templateEntry(section, index, level) {
    const nElementSubentry = Array.from(section.querySelectorAll(':scope > component-section'));
    // TODO: Use functional style.
    let elementEntry = html``;
    if (section === this.sectionRoot) {
      // ToC root is not a level
      let indexChild = 0;
      elementEntry = html`<li>
        ${nElementSubentry.map(elementSubentry => {
          indexChild += 1;
          return html`${this.templateEntry(elementSubentry, indexChild, level)}`;
        })}
      </li>`;
    } else if (nElementSubentry.length > 0) {
      let indexChild = 0;
      const levelNew = level + 1;
      elementEntry = html`<li>
        <details>
          <summary>${Tableofcontents.templateEntryLeaf(section, index, level)}</summary>
          ${nElementSubentry.map(elementSubentry => {
            indexChild += 1;
            return html`${this.templateEntry(elementSubentry, indexChild, levelNew)}`;
          })}
        </details>
      </li>`;
    } else if (index) {
      elementEntry = html`<li>${Tableofcontents.templateEntryLeaf(section, index, level)}</li>`;
    }
    return html`<ol>
      ${elementEntry}
    </ol>`;
  }

  static get properties() {
    return { active: { type: Boolean } };
  }

  static finalizeStyles(_styles) {
    return [
      css`
        @keyframes slide-out-left {
          from {
            transform: translate3d(0, 0, 0);
          }

          to {
            visibility: hidden;
            transform: translate3d(-100%, 0, 0);
          }
        }

        @keyframes slide-in-left {
          from {
            visibility: visible;
            transform: translate3d(-100%, 0, 0);
          }

          to {
            transform: translate3d(0, 0, 0);
          }
        }

        #buttons {
          display: flex;
          align-items: left;
          margin: 0.2em;
        }

        button {
          margin: 0.1em;
          padding: 0.25em 0.375em;
          border: 0;
          background-color: transparent;
          color: inherit;
          font: inherit;
          font-size: 1.5em;
          letter-spacing: inherit;
          cursor: pointer;
        }

        #button-up {
          margin: 0 1em;
          padding: 0 10px;
          font-size: revert;
        }

        nav {
          left: 0;
          z-index: 1;
          display: flex;
          flex-direction: column;
          min-width: min-content;
          max-width: max-content;
          max-height: 100%;
          background-color: transparent;
          backdrop-filter: blur(15px);
          scrollbar-width: thin;
        }

        .locator-toc {
          font-weight: bold;
        }

        nav > #tableofcontents {
          display: none;
          padding: 0.3rem;
          animation: slide-out-left;
          animation-duration: 0.2s;
        }

        nav.active > #tableofcontents {
          display: revert;
          animation: slide-in-left;
          animation-duration: 0.2s;
        }

        ol {
          margin-left: 0;
          padding: 10px;
          list-style: none;
          padding-block: 5px;
        }

        li > p {
          margin-block: 0;
        }

        summary::-webkit-details-marker {
          color: gray;
        }

        #tableofcontents li > ol:nth-of-type(odd) {
          background: rgba(0 0 0 / 2.5%);
        }

        details > summary {
          width: 100%;
          line-height: 1.5;
          cursor: pointer;
        }

        details summary > * {
          display: inline;
        }

        details[open] > *:not(summary) {
          animation: slide-in-left;
          animation-duration: 0.4s;
        }

        details > summary:focus {
          outline: unset;
        }

        details > summary:not(:hover) {
          opacity: 0.7;
        }

        /* #region: Hyperlinks */
        a,
        *::part(a) {
          color: var(--color-hyperlink);
          font-weight: normal;
          text-decoration: none;
          text-decoration-color: #b3b8c4;
          text-decoration-line: none;
          text-decoration-skip: none;
          text-decoration-skip-ink: none;
          text-decoration-thickness: from-font;
        }

        *::part(a):hover,
        a:hover {
          text-decoration-color: var(--color-hyperlink-hover) !important;
        }

        /* #endregion */
      `,
    ];
  }

  _handleClick() {
    this.classes.active = !this.active;
    this.active = !this.active;
  }

  // TODO:
  // Side-by-side if enough space in viewport.
  // Top-before-bottom otherwise.
  // Folded by default.
  // Sticky or static.
  render() {
    // TODO: stylelint whitelisting versus all custom components
    // TODO: Handle case where `sectionRoot` is `null` or >1.
    return html`<nav class="${classMap(this.classes)}">
      <div id="buttons">
        <button type="button" @click="${this._handleClick}" title="Menu">☰</button>
      </div>
      <div id="tableofcontents">
        <button type="button" id="button-up" title="Omhoog"><a href="..">⌃</a></button>
        ${this.templateEntry(this.sectionRoot, 0, 2)}
      </div>
    </nav>`;
  }
}

export const componentTableofcontents = () => {
  customElements.define('component-tableofcontents', Tableofcontents);
};

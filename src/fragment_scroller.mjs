import { Stack } from './stack.js';

export const locationHashChanged = async event => {
  if (event) {
    event.preventDefault();
  }
  // eslint-disable-next-line no-restricted-globals
  const hash = location.hash.substring(1);
  const target = document.getElementById(hash);
  const stackElementScroll = new Stack();
  if (target) {
    stackElementScroll.push(target);
    const optionsScroll = { behavior: 'smooth' };
    // Open all `<details>` elements.
    let ancestor = target.closest('details');
    let ancestorNew;
    while (ancestor !== null) {
      // Scroll `<details>` element into view.
      ancestorNew = ancestor.parentElement.closest('details');
      if (ancestor === ancestorNew) {
        ancestor = null;
      } else {
        ancestor.setAttribute('open', '');
        stackElementScroll.push(ancestor);
        ancestor = ancestorNew;
      }
    }
    for (const elementScroll of stackElementScroll) {
      elementScroll.scrollIntoView(optionsScroll);
    }
  } else if (hash) {
    // eslint-disable-next-line no-console
    console.error(`Fragment ID unknown: ${hash}`);
  }
};

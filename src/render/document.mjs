import { componentExercise } from '../component/exercise.js';
import { componentHeading } from '../component/heading.js';
import { componentIntendedlearningoutcome } from '../component/intendedlearningoutcome.js';
import { componentGradingcriteria } from '../component/gradingcriteria.js';
import { componentGradingcriterion } from '../component/gradingcriterion.js';
import { componentIntendedlearningoutcomes } from '../component/intendedlearningoutcomes.js';
import { componentMiroboard } from '../component/miroboard.js';
import { componentSection } from '../component/section.js';
import { componentTableofcontents } from '../component/tableofcontents.js';
import { componentVideo } from '../component/video.js';
import { renderAll } from './all.js';
import { locale } from '../citationlocale.js';
import { citationstyleIeee } from '../citationstyle.js';
import { processCitations } from '../process_citations.js';
import '../css/style.css';
import '../css/biblio.css';

export const renderDocument = (citationData, urlBase) => {
  window.addEventListener(
    'DOMContentLoaded',
    () => {
      componentSection(); // TODO: order dependence
      componentTableofcontents();
      componentIntendedlearningoutcome();
      componentIntendedlearningoutcomes();
      componentGradingcriterion();
      componentGradingcriteria();
      componentVideo();
      componentMiroboard();
      componentExercise();
      componentHeading();
      if (citationData !== undefined && citationData !== null && citationData.items.length > 0) {
        processCitations(citationData, locale, citationstyleIeee);
      }
      const elementBody = document.body;
      renderAll(elementBody, urlBase);
    },
    false,
  );
};

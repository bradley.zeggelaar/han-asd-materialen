export const renderPractice = elementBody => {
  const nElementPraktijk = elementBody.querySelectorAll(':scope .praktijk');
  for (const elementPraktijk of nElementPraktijk) {
    elementPraktijk.prepend('🛠️  ');
  }
};

import Prism from 'prismjs';

import { renderFigurenumbering } from './figurenumbering.js';
import { renderMath } from './math.js';
import { renderPractice } from './practice.js';
import { renderSource } from './source.js';
import { renderTablenumbering } from './tablenumbering.js';
import { locationHashChanged } from '../fragment_scroller.js';

import 'sanitize.css/assets.css';
import 'sanitize.css/reduce-motion.css';
import 'sanitize.css/typography.css';

import 'prismjs/components/prism-java';
import 'prismjs/components/prism-rust';
import 'prismjs/plugins/line-highlight/prism-line-highlight.css';
import 'prismjs/plugins/line-highlight/prism-line-highlight.js';
import 'prismjs/plugins/show-language/prism-show-language.js';
import 'prismjs/plugins/toolbar/prism-toolbar.css';
import 'prismjs/plugins/toolbar/prism-toolbar.js';
import 'prismjs/themes/prism-dark.css';

export const renderAll = (elementBody, urlBase) => {
  renderPractice(elementBody);
  renderSource(elementBody);
  renderTablenumbering(elementBody);
  renderFigurenumbering(elementBody);
  Prism.highlightAll();
  renderMath(urlBase);
  const elementMain = elementBody.querySelector('main');
  elementMain.hidden = false;
  locationHashChanged();
  window.addEventListener('hashchange', locationHashChanged);
};

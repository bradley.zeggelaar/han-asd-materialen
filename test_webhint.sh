#!/bin/sh
set -euxo pipefail

while true; do
  if nc -z localhost 8080; then
    pnpm hint --debug --telemetry=off 'https://localhost:8080'
  else
    echo 'Waiting for Snowpack dev server ...'
    sleep 2
  fi
done

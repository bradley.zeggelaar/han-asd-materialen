<!-- LTeX: language=en -->
<!-- omit in toc -->
# Materials for the Advanced Software Development module

- [Content authoring](#content-authoring)
  - [Advantages of Pug over HTML](#advantages-of-pug-over-html)
- [Development](#development)
  - [Installation](#installation)
  - [`pnpm start -- --reload`](#pnpm-start------reload)
  - [`pnpm build`](#pnpm-build)

## Content authoring

Edit the files under [`public/`](public).
The `.pug` files are [Pug](https://pugjs.org/language/tags.html) templates.
Split up large HTML documents.
Use the web components defined in this projects for standard information parts, such as `<component-section>`.

For examples, see [`public/ADP`](public/ADP).

### Advantages of Pug over HTML

- Supports conditional logic.
- Supports reuse of fragments.
- Less obtrusive syntax.
- Supports variables, including environment variables.
That supports conditional rendering with a CI pipeline without server-side support.
Example use cases: instructor versus student editions.
- Is rendered to optimized HTML, e.g., without comments.

## Development

### Installation

Install the dependencies in the local `node_modules/` folder.

```sh
pnpm install --frozen-lockfile
```

### `pnpm start -- --reload`

Runs the app in the development mode.
Open https://localhost:8080 to view it in the browser.
You may want to add the certificate to the trust store.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `pnpm build`

Builds a copy of the website in the `build/` folder.
